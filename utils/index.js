import {notification} from "antd";
import cookie from "js-cookie";
import axios from "axios";

export const openNotificationWithIcon = (type, message) => {
    notification[type]({
        message: message,
        duration: 8,
        top: 80
    });
}

export const sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}
export const secondsToHms = (time) => {
    time = Number(time);
    const h = Math.floor(time / 3600);
    const m = Math.floor((time % 3600) / 60);
    const mDisplay = m > 0 ? m + (m === 1 ? " min " : " mins ") : "";
    const hDisplay = h > 0 ? h + (h === 1 ? " hour " : " hours ") : "";
    return hDisplay + mDisplay;
};

export const secondsMs = (time) => {
    time = Number(time);
    const m = Math.floor((time % 3600) / 60);
    const s = Math.floor((time % 3600) % 60);
    const mDisplay = m > 0 ? m + (m === 1 ? " min " : " mins ") : "";
    return mDisplay + s;
};

export const getFromLocalstorage = (key) => {
    if (process.browser) {
        return JSON.parse(localStorage.getItem(key));
    }
};
export const storeToLocalstorage = (key, value) => {
    if (process.browser) {
        localStorage.setItem(key, JSON.stringify(value));
    }
};
export const removeFromLocalstorage = (key) => {
    if (process.browser) {
        localStorage.removeItem(key);
    }
};

export const setCookie = (key, value) => {
    if (process.browser) {
        cookie.set(key, value,);
    }
};

export const removeCookie = (key) => {
    if (process.browser) {
        cookie.remove(key);
    }
};
export const isAuthenticated = (data, next) => {
    setCookie("token", data.token);
    storeToLocalstorage("user", data.user)
    next();
};

export const getCookie = (key, req) => {
    return process.browser ? getCookieFromBrowser(key) : getCookieFromServer(key, req);
};

export const getCookieFromBrowser = key => {
    return cookie.get(key);
};

export const getCookieFromServer = (key, req) => {
    if (req !== undefined) {
        if (!req.headers.cookie) {
            return undefined;
        }

        let token = req.headers.cookie.split(';').find(c => c.trim().startsWith(`${key}=`));
        if (!token) {
            return {};
        }
        return token.split('=')[1];
    }

};

export const getUser = () => {
    const cookieChecked = getCookie("token");
    if (cookieChecked) {
        if (getFromLocalstorage("user") !== 'undefined' && getFromLocalstorage("user") !== '') {
            return getFromLocalstorage("user")
        }
    } else {
        return null
    }

};

export const getCart = () => {
    if (getFromLocalstorage("cart") !== 'undefined' && getFromLocalstorage("cart") !== '') {
        return getFromLocalstorage("cart")
    } else {
        return null
    }
};

export const isLogin = () => {
    return getUser();
};

export const getErrors = async (response) => {
    let errors = []
    const json = await response.json();
    if (response.status === 422) {
        Object.entries(json.message)
            .forEach(([key, value]) => errors.push(value));
    } else {
        errors.push(json.message)
    }

    return errors;
}

export const fetcher = (url, token) =>
    axios
        .get(url, {headers: {Authorization: "Bearer " + token}})
        .then((res) => res.data);