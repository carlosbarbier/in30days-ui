import React from 'react';
import InvoiceList from "./InvoiceList";
import PaymentInformation from "./PaymentInformation";

const InvoiceComponent = () => {
    return (
        <div className="invoice-wrapper bg-white ">
            <div className="row">
                <div className="col-md-7">
                    <InvoiceList/>
                </div>
                <div className="col-md-5 border-start mh-100">
                    <PaymentInformation/>
                </div>
            </div>
        </div>
    );
};

export default InvoiceComponent;