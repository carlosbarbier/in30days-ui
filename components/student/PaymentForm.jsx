import React, {Component, useState} from 'react';
import Cards from 'react-credit-cards';
import {Button, Form, Input} from "antd";

class PaymentForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cvc: '',
            expiry: '',
            focus: '',
            name: '',
            number: '',
            errors: null,
            loading: false,
            success: false
        };

        this.handleInputFocus = this.handleInputFocus.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputFocus = (e) => {
        this.setState({focus: e.target.name});
    }

    handleInputChange = (e) => {
        const {name, value} = e.target;
        this.setState({[name]: value});
    }
    handleSubmit = async (e) => {
        e.preventDefault()
    }
    render() {
        return (
            <div className="card-payment-form">
                <div className="my-4">
                    <Cards
                        cvc={this.state.cvc}
                        expiry={this.state.expiry}
                        focused={this.state.focus}
                        name={this.state.name}
                        number={this.state.number}
                    />
                </div>
                <div className="row g-3 mb-2">
                    <div className="col-sm-12">
                        <label className="form-label">Name on Card</label>
                        <input type="text" className="form-control"
                               name="name"
                               required=""
                               inputMode="text"
                               onChange={this.handleInputChange}
                               onFocus={this.handleInputFocus}
                        />
                    </div>
                </div>
                <div className="row g-1">
                    <div className="col-sm-7">
                        <label className="form-label">Card Number</label>
                        <input type="text" className="form-control"
                               inputMode="numeric"
                               pattern="[0-9\s]{13,19}"
                               autoComplete="number"
                               maxLength="19"
                               name="number"
                               onChange={this.handleInputChange}
                               onFocus={this.handleInputFocus}
                        />
                    </div>
                    <div className="col-sm">
                        <label className="form-label">Expiry</label>
                        <input type="text" className="form-control"
                               name="expiry"
                               onChange={this.handleInputChange}
                               onFocus={this.handleInputFocus}
                               maxLength="4"
                               inputMode="numeric"
                               required=""
                        />
                    </div>
                    <div className="col-sm">
                        <label className="form-label">Cvc</label>
                        <input type="text" className="form-control"
                               name="cvc"
                               onChange={this.handleInputChange}
                               onFocus={this.handleInputFocus}
                               required=""
                               inputMode="numeric"
                               maxLength="3"
                        />
                    </div>
                </div>
                <div className="d-flex justify-content-end my-1">
                    <button type="button" className="btn btn-light">Save</button>
                </div>

            </div>

        );
    }
}
export default PaymentForm