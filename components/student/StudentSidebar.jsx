import React from 'react';

const StudentSidebar = () => {
    return (
        <nav className="navbar navbar-expand-md  mb-4 rounded-3">
            <div className="collapse navbar-collapse">
                <div className="navbar-nav flex-column">
                    <span className="navbar-header fw-bolder ">Payments</span>
                    <ul className="list-unstyled mb-4">
                        <li className="nav-item">
                            <a className="nav-link" href="#"> <i className="bi bi-calendar3-event  mr-2"/>{""} Billing
                                Info</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#"> <i className="bi bi-receipt mr-2"/> Payment</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#"> <i className="bi bi-calendar3-event mr-2"/> Invoice</a>
                        </li>
                    </ul>

                    <span className="navbar-header fw-bolder">Account Settings</span>
                    <ul className="list-unstyled ms-n2 mb-0">
                        <li className="nav-item">
                            <a className="nav-link" href="#"><i className="bi bi-gear mr-2"/> {""}Edit Profile</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#"><i className="bi bi-file-lock mr-2"/>{""} Security</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#"><i className="bi bi-bell mr-2"/>{""} Notifications</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#"><i className="bi bi-file-earmark-lock mr-2"/>{""} Profile
                                Privacy</a>
                        </li>

                        <li className="nav-item">
                            <a className="nav-link" href="#"> <i className="bi bi-archive mr-2"/>{""} Delete
                                Profile</a>
                        </li>

                        <li className="nav-item">
                            <a className="nav-link " href="#"> <i className="bi bi-folder-symlink"/> {""} Linked
                                Accounts</a>
                        </li>

                        <li className="nav-item">
                            <a className="nav-link" href="#"><i className="bi bi-box-arrow-right"/> Sign Out</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


    );
};

export default StudentSidebar;