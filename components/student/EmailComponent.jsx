import React, {useState} from 'react';
import {Button, Form, Input, Upload} from "antd";
import {sleep} from "../../utils";
import {UploadOutlined} from "@ant-design/icons";

const layout = {
    labelCol: {
        span: 24,
    },
    wrapperCol: {
        span: 24,
    },
};
const tailLayout = {
    wrapperCol: {
        offset: 0,
        span: 24,
    },
};
const EmailComponent = () => {

    const [form] = Form.useForm();
    const [loading, setLoading] = useState(false)
    const [errors, setErrors] = useState(null)
    const [success, setSuccess] = useState(false)

    const onFinish = async (user) => {
        setLoading(true)
        await sleep(1500)

    };
    const props = {
        name: 'file',
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        headers: {
            authorization: 'authorization-text',
        },
        onChange(info) {

        },
    };
    return (

            <div className="profile-wrapper p-4">
                <div className="row">
                    <div className="col-md-6">
                        <div className="bg-white p-3">
                            <div className="profile-picture">
                                <div className="row">
                                    <div className="col-md-3">
                                        <img className="rounded-circle" width="60px"
                                             src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg" alt="img"/>
                                    </div>
                                    <div className="col">
                                       <div className="d-flex flex-column align-items-end mt-4">
                                           <Upload {...props}>
                                               <Button icon={<UploadOutlined />}>Upload new picture</Button>
                                           </Upload>
                                       </div>

                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <Form
                                {...layout}
                                form={form}
                                name="update_profile"
                                onFinish={onFinish}
                                initialValues={{
                                    email: "kouakou@gmail.com",
                                    name: "Kouakou",
                                }}
                            >
                                <div style={{marginTop: "2%", marginBottom: "4%"}}>
                                    <Form.Item
                                        label="Name"
                                        name="name"
                                        className="my-2"
                                    ><Input/>

                                    </Form.Item>
                                    <Form.Item
                                        label="Email"
                                        name="email"
                                        className="my-2"
                                        rules={[
                                            {
                                                type: "email"
                                            },
                                        ]}
                                    ><Input/>
                                    </Form.Item>
                                    <Form.Item {...tailLayout}>
                                        <Button type="text" htmlType="submit" disabled={loading}
                                                className='btn-default mt-2'>
                                            {loading
                                                ? <><span className="spinner-border spinner-border-sm mx-1"
                                                          role="status" aria-hidden="true"/> Registering...</>
                                                : `Edit Information`}
                                        </Button>
                                    </Form.Item>

                                </div>
                            </Form>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="bg-white p-3">
                            <Form
                                {...layout}
                                form={form}
                                name="update_password"
                                onFinish={onFinish}
                                initialValues={{
                                    password: "12345678",
                                }}
                            >
                                <div style={{marginTop: "2%", marginBottom: "4%"}}>
                                    <Form.Item
                                        label="Password"
                                        name="password"
                                        className="mt-2"
                                    >
                                        <Input.Password disabled />
                                    </Form.Item>
                                    <Form.Item {...tailLayout}>
                                        <Button type="text" htmlType="submit" disabled={loading}
                                                className='btn-default mt-2'>
                                            {loading
                                                ? <><span className="spinner-border spinner-border-sm mx-1"
                                                          role="status" aria-hidden="true"/> Registering...</>
                                                : `Change Password`}
                                        </Button>
                                    </Form.Item>

                                </div>
                            </Form>
                        </div>


                    </div>
                </div>
            </div>

    );
};

export default EmailComponent;