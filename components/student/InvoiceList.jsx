import React from 'react';

const InvoiceList = () => {
    return (
        <div className="bg-white p-3">
            <table className="table">
                <thead>
                <tr>
                    <th scope="col" className="fw-normal text-muted">Invoice</th>
                    <th scope="col" className="fw-normal text-muted">Issue Date</th>
                    <th scope="col" className="fw-normal text-muted">Status</th>
                    <th scope="col" className="fw-normal text-muted">Total</th>
                    <th scope="col" className="fw-normal text-muted">Download</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th className="fw-normal">341951</th>
                    <td>August 11, 2021</td>
                    <td>Paid</td>
                    <td>€35.0</td>
                    <td>
                        <span><i className="bi bi-download"/> Download</span>
                    </td>
                </tr>
                <tr>
                    <th className="fw-normal">341951</th>
                    <td>August 11, 2021</td>
                    <td>Paid</td>
                    <td>€35.0</td>
                    <td>
                        <span><i className="bi bi-download"/> Download</span>
                    </td>
                </tr>
                <tr>
                    <th className="fw-normal">341951</th>
                    <td>August 11, 2021</td>
                    <td>Paid</td>
                    <td>€35.0</td>
                    <td>
                        <span><i className="bi bi-download"/> Download</span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    );
};

export default InvoiceList;