import React from 'react';

const StudentContentArea = ({children}) => {
    return (
        <>
            {children}
        </>
    );
};

export default StudentContentArea;