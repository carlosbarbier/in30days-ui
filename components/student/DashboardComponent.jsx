import React from 'react';
import Link from "next/link";

const DashboardComponent = ({courses}) => {

    return (
        <div className="card bg-white border-0 rounded-0 dashboard-content py-5 px-5">
            <p className="h4 mb-4">My Courses</p>
            {courses && courses.map((course, index) => (
                <div className="course" key={index}>
                    <div className="d-flex justify-content-between  mb-3 px-5  shadow-sm">
                        <div className="d-flex flex-row align-items-center ">
                            <img src={`${course.course.url}`} className="img-fluid rounded-3" alt="Image"
                                 style={{width: "130px", height: "130px"}}/>
                        </div>
                        <div className="d-flex flex-row align-items-center">
                            <h6>{course.course.title} </h6>
                        </div>
                        <div className="d-flex flex-row align-items-center">
                            <div>
                                <Link href="/watch/[slug]"
                                      as={`/watch/${course.course.slug}`}>
                                    <a href="" className="btn btn-blue-1 btn-sm fw-bold">
                                        Go to my course
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            ))}
        </div>


    );
};

export default DashboardComponent;