import React, {useState} from 'react';
import PaymentForm from "./PaymentForm";

const PaymentInformation = () => {
    const [show,setShow]= useState(false)
    const handleShow=() =>{
      setShow(true)
    }

    return (
        <div className="bg-white payment-wrapper p-3">
            <div className="edit-card-info my-2 ">
                <h6 className="fs-4">Billing information</h6>
            </div>
            <div className="edit-payment">
                <div>
                    <span className="fs-6">Saved cards</span>
                    <div className="row ">
                        <div className="col-2 col-xs-2">
                            <img className="img-fluid" src="https://img.icons8.com/color/48/000000/mastercard-logo.png" alt="img"/>
                        </div>
                        <div className="col-7 col-xs7 pt-1"><input type="text" placeholder="**** **** **** 3193" disabled/></div>
                        <div className="col-3 col-xs-3 pt-1 d-flex justify-content-center"><a>Remove </a></div>
                    </div>
                    {!show && <p className="mt-1"><a onClick={handleShow}>Update Card information</a> </p>}

                </div>
            </div>
            <div className="payment-element">
                {show && <PaymentForm/> }
            </div>

        </div>
    );
};

export default PaymentInformation;