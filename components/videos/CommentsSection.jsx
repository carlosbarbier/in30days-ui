import React from 'react';

const CommentsSection = () => {

    return (

        <div className="container mt-5 mb-5">
            <div className="d-flex row">
                <div className="d-flex flex-column col-md-9 col-9">
                    <div
                        className="d-flex flex-row align-items-center text-left comment-top p-2 bg-white border-bottom px-4">
                        <h4>Comments</h4>
                    </div>
                    <div className="coment-bottom bg-white p-2 px-4">
                        <div className="row ">
                            <div className="col-md-12">
                                <div className="mb-3">
                                    <textarea className="form-control w-100" rows="3"/>
                                </div>
                            </div>
                        </div>
                        <div className="d-flex ">
                            <button type="submit" className="btn btn-light">Ask a question </button>
                        </div>
                        <div className="commented-section mt-2">
                            <div className="d-flex flex-row align-items-center ">
                                <span className="mr-2 fw-bold author">Corey< /span>
                                <span className="mx-2 fw-lighter">4 hours ago</span>
                            </div>
                            <p className="">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                            </p>
                            <div className="reply-section">
                                <a className="btn btn-white p-0 mt-1 fw-light"> <i className="bi bi-reply"/>Reply</a>
                            </div>
                        </div>
                        <div className="commented-section commented-reply mt-2">
                            <div className="d-flex flex-row align-items-center ">
                                <span className="mr-2 fw-bold author">Kouakou Carlos< /span>
                                <span className="mx-2 fw-lighter">4 hours ago</span>
                            </div>
                            <p className="h6">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                            </p>
                            <div className="reply-section">
                                <a className="btn btn-white p-0 mt-1 fw-light"> <i className="bi bi-reply"/>Reply</a>
                            </div>
                        </div>
                        <div className="commented-section commented-reply mt-2">
                            <div className="d-flex flex-row align-items-center ">
                                <span className="mr-2 fw-bold author">Paul Hayes< /span>
                                <span className="mx-2 fw-lighter">4 hours ago</span>
                            </div>
                            <p className="h6 pb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </p>
                            <div className="reply-section fw-light">
                                <a className="btn btn-white p-0 mt-1 fw-light"> <i className="bi bi-reply"/>Reply</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    );
};

export default CommentsSection;