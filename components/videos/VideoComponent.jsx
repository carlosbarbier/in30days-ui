import React, {useEffect, useState} from 'react';
import ReactPlayer from "react-player";
import {Collapse} from "antd";
import {finishToWatchVideo} from "../../api/student/course";
import {secondsMs} from "../../utils";

const {Panel} = Collapse;
const VideoComponent = ({sections, ids, defaultUrl, slug}) => {
    const [url, setUrl] = useState(defaultUrl)
    const [playing, setPlaying] = useState(true)
    const [id, setId] = useState(sections[0].videos[0].id)
    const [videosAlreadyWatchId, setVideosAlreadyWatchId] = useState([])
    useEffect(() => {
        let array = [];
        ids.map((video) => {
            array.push(video.video_id);
        });
        setVideosAlreadyWatchId(array)
    }, []);
    const handleToWatch = (e) => {
        const id = e.currentTarget.dataset.id;
        const url = e.currentTarget.dataset.url;
        const index = e.currentTarget.dataset.section_index;
        setUrl(url)
        setId(id)
        setPlaying(true)
    };


    const handleEnded = async () => {
        console.log(id)
        const video_id = parseInt(id);
        if (!videosAlreadyWatchId.includes(video_id)) {
            let array = videosAlreadyWatchId;
            array.push(video_id);
            setVideosAlreadyWatchId(array)
            await finishToWatchVideo(slug, video_id);
        }

    }
    return (
        <div className="container my-3">
            <div className="row d-flex video-wrapper">
                <div className="col-lg-9">
                    <div className="player-wrapper">
                        <ReactPlayer
                            className="react-player"
                            url={url}
                            width="100%"
                            height="100%"
                            controls={true}
                            muted={false}
                            playing={playing}
                            onEnded={handleEnded}
                            onError={() => console.log("Error")}

                        />
                    </div>

                </div>
                <div className="col-lg-3 ">
                    <div className="video-sidebar border-0">
                        <Collapse bordered={false} defaultActiveKey={['0']} className="bg-white">
                            {sections && sections.map((section, index) => (
                                <Panel header={`DAY ${index + 1} - ${section.wording}`} key={index}
                                       className=" border-0">
                                    {section.videos && section.videos.map(video => (
                                        <div
                                            className={
                                                parseInt(id) === video.id
                                                    ? " row course-curriculum  course-curriculum-active px-0 py-1"
                                                    : " row course-curriculum px-0 py-1"
                                            }
                                            key={video.id}
                                            onClick={handleToWatch}
                                            data-url={video.url}
                                            data-id={video.id}
                                            data-section_index={index}

                                        >
                                            <div className="col-9 ">

                                                    <span className=" rounded-circle me-2 player-indicator icon-shape">
                                                    <i
                                                        className={
                                                            parseInt(id) === video.id
                                                                ? "bi bi-play-fill playing-icon"
                                                                : "bi bi-play"
                                                        }
                                                    />
                                                </span>
                                                {video.title}
                                            </div>
                                            <div className="col-3 pt-1">
                                                <div className="course-curriculum-time d-flex justify-content-evenly">
                                                    <i
                                                        className={
                                                            videosAlreadyWatchId.includes(video.id)
                                                                ? "bi bi-check-lg"
                                                                : ""
                                                        }
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    ))}

                                </Panel>
                            ))}

                        </Collapse>
                    </div>
                </div>

            </div>
        </div>


    );
};

export default VideoComponent;