import React from 'react';
import ReactPlayer from "react-player";

const VideoPlayer = () => {

    const findNextVideo = (id) => {

    };
    const handleToWatch = (e) => {

    };
    const handleEnded = async () => {

    }
    return (
        <div className="player-wrapper">
            <ReactPlayer
                className="react-player"
                url="./video1.mp4"
                width="100%"
                height="100%"
                controls={true}
                muted={false}
                playing={true}
                onEnded={handleEnded}
                onError={() => console.log("Error")}

            />
        </div>
    );
};

export default VideoPlayer;