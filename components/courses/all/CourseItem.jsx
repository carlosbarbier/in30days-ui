import React from 'react';
import Link from "next/link";

const CourseItem = () => {
    return (
        <div className="container">
            <div className="course-details-header main-banner   rounded-3  my-3 px-3 py-3 ">
                <div className="row align-items-center ">
                    <div className=" col-md-12 pb-3">
                        <h1 className="display-6 fw-bold my-4 text-white">All available Courses</h1>
                    </div>
                </div>
            </div>
            <div id="course-list">
                <div className="row">
                    {courses && courses.map(course => (
                        <div className="col-12 my-2" key={course.slug}>
                            <div className="card bg-white rounded-3  p-2 border-0">
                                <div className="row g-0">
                                    <div className="col-md-3">
                                        <img src={course.url} className="rounded-2 img-fluid"
                                             alt="Card image"
                                             style={{width: "100%", height: "100%", objectFit: "contain"}}/>
                                    </div>
                                    <div className="col-md-7">
                                        <div className="card-body">
                                            <h5 className="card-title"><a href="#"
                                                                          className="text-decoration-none text-black">{course.title}</a>
                                            </h5>
                                            <p className="text-truncate text-muted">{course.description} </p>
                                            <ul className="list-inline mb-2">
                                                <li className="list-inline-item h6 fw-light mb-1 mb-sm-0"><i
                                                    className="bi bi-clock me-2"/>6h 56m
                                                </li>
                                                <li className="list-inline-item h6 fw-light text-muted mb-1 mb-sm-0">
                                                    <i className="bi bi-table fw-bold me-2"/>82 lectures
                                                </li>

                                            </ul>
                                            <div
                                                className="d-sm-flex mt-5 justify-content-sm-between align-items-center">
                                                <div className="d-flex align-items-center">
                                                    <p className="mb-0"><a href="#"
                                                                           className="h6 text-muted fw-normal">Larry
                                                        Lawson</a></p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-2">
                                        <div
                                            className="m-5 mt-sm-0 position-absolute top-0 end-0 h3 ">
                                            £12
                                        </div>
                                        <div className="mt-3 mt-sm-0 position-absolute bottom-0 end-0 m-3">
                                            <Link href="/courses/[slug]"
                                                  as={`/courses/${course.slug}/`}>
                                                <a href="" className="btn btn-primary fw-bold">
                                                    View more
                                                </a>
                                            </Link>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    ))}


                </div>
            </div>
        </div>
    );
};

export default CourseItem;