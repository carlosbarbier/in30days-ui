import React from 'react';

const CurriculumItem = ({video}) => {
    return (
        <div className="course-curriculum d-flex justify-content-between rounded-3 my-3  bg-white">
            <p><i className="bi bi-play"/> {video.title}</p>
            <div className="course-curriculum-time d-flex justify-content-between">
                <span className="icon-wrapper"><i className="bi bi-clock"/></span>
                <span className="icon-timer">05min</span>
            </div>
        </div>
    );
};

export default CurriculumItem;