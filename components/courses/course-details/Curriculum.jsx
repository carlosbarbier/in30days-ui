import React from 'react';
import {Collapse} from "antd";
import SectionItem from "../../teacher/section/SectionItem";
import CourseDetailsSectionItem from "./CourseDetailsSectionItem";

const {Panel} = Collapse;


const Curriculum = ({sections}) => {

    return (
        <div className="my-3 bg-white">
            {sections && sections.map(section => (
                <Collapse bordered={false} ghost={true} key={section.id} expandIconPosition="left"
                          className="rounded px-4 pt-1 pb-1">
                    <Panel header={section.wording}>
                        <div className=" course-section" key={section.id}>
                            <CourseDetailsSectionItem section={section}/>
                        </div>
                    </Panel>
                </Collapse>

            ))}
        </div>
    );
};

export default Curriculum;