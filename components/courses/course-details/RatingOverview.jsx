import React from 'react';
import {Progress} from "antd";

const RatingOverview = () => {

    return (
        <div className="mb-3 rating-overview px-3">
            <h4 className="mb-4">How students rated this courses</h4>
            <div className="row align-items-center ">
                <div className="col-auto text-center">
                    <h3 className="display-2 total-rating fw-bold">4.5</h3>
                    <i className="bi bi-star text-warning"/>
                    <i className="bi bi-star text-warning"/>
                    <i className="bi bi-star text-warning"/>
                    <i className="bi bi-star text-warning"/>
                    <i className="bi bi-star text-warning"/>
                    <p className="mb-0 fs-6">(Based on 27 reviews)</p>
                </div>
                <div className="col order-3 order-md-2 order-sm-2 ">
                    <Progress percent={30} showInfo={false} strokeColor="#FFAA46"/>
                    <Progress percent={40} showInfo={false} strokeColor="#FFAA46"/>
                    <Progress percent={59} showInfo={false} strokeColor="#FFAA46"/>
                    <Progress percent={90} showInfo={false} strokeColor="#FFAA46"/>
                    <Progress percent={70} showInfo={false} strokeColor="#FFAA46"/>
                </div>
                <div className="col-md-auto col-6 order-2 order-md-3">
                    <div>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <span className="ms-1">53%</span>
                    </div>
                    <div>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <span className="ms-1">36%</span>
                    </div>
                    <div>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <span className="ms-1">9%</span>
                    </div>
                    <div>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <span className="ms-1">3%</span>
                    </div>
                    <div>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <span className="ms-1">2%</span>
                    </div>
                </div>
            </div>
        </div>


    );
}

export default RatingOverview;