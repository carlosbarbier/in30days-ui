import React from 'react';
import RatingOverview from "./RatingOverview";
import Ratings from "./Ratings";

const DetailsReview = () => {
    return (
        <section id="reviews-section" className="reviews-section pt-4 rounded-3 my-3  bg-white">
            <div className="rating-overview">
                <RatingOverview/>
            </div>
            <div className="reviews pb-5 pt-4">
                <Ratings/>
            </div>
        </section>
    );
};

export default DetailsReview;