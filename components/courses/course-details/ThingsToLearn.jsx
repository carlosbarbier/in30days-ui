import React from 'react';

const ThingsToLearn = ({description,things_to_learn}) => {
    return (

        <div className="course-details rounded-3 pt-4 p-3  bg-white mb-4 ">
            <div className="description my-3 ">
                <h6 className="fw-bold ">Course Descriptions</h6>
                <p className="h6">
                    {description}
                </p>

            </div>
            <div className="to-learn-card border-0">
                <h6 className="fw-bold">What you'll learn?</h6>
                <ul className="list-items d-flex flex-column  mt-3 h6">
                    {things_to_learn && things_to_learn.map((item,key)=>(
                            <li key={key} className="">
                                <i className="bi bi-check"/>
                                {item}
                            </li>
                    ))}
                    <li>
                        <i className="bi bi-check" />
                        Identify the fundaments of composing a successful close.
                    </li>
                    <li>
                        <i className="bi bi-check"/>
                        Explore how to connect with your audience through crafting compelling stories.
                    </li>
                    <li>
                        <i className="bi bi-check"/>
                        Examine ways to connect with your audience by personalizing your content.
                    </li>
                    <li>
                        <i className="bi bi-check"/>
                        Explore how to communicate the unknown in an impromptu communication.
                    </li>
                    <li>
                        <i className="bi bi-check"/>
                        Break down the best ways to exude executive presence.
                    </li>
                    <li>
                        <i className="bi bi-check"/>
                        Break down the best ways to exude executive presence.
                    </li>
                    <li>
                        <i className="bi bi-check"/>
                        Break down the best ways to exude executive presence.
                    </li>
                </ul>
            </div>
            <div className="prerequis">
                <h6 className="fw-bold">Course Prerequisite</h6>
                <p className="h6">
                    Recognize the importance of understanding your objectives when addressing an audience
                </p>
            </div>

        </div>
    );
};

export default ThingsToLearn;