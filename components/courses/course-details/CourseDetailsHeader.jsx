import React from 'react';
import moment from "moment";

const CourseDetailsHeader = ({title, total_student, last_updated, author}) => {
    return (
        <div className="course-details-header bg-white  rounded-3  my-3 px-3 py-5">
            <div className="row align-items-center ">
                <div className=" col-md-12 pb-3">
                    <h1 className="display-3 fw-bold my-4">{title}</h1>
                    <div className="d-flex align-items-center">
                        <span className="ms-3"><i
                            className="bi bi-person"/> {total_student = 0 ? "No student enrolled yet" : `${total_student} Students Enrolled`}  </span>
                        <span className="ms-3"><i
                            className="bi bi-clock"/> Last updated: {moment(last_updated).fromNow()} </span>
                        <span className="ms-3"> <i className="bi bi bi-person me-1"/>Author :{author}</span>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CourseDetailsHeader;