import React from 'react';
import {secondsMs} from "../../../utils";

const CourseDetailsSectionItem = ({section}) => {

    return (
        <div className="list-group list-group-flush ">
            {section.videos && section.videos.map(video => (
                <div className="course mb-0" key={video.id}>
                    <div className="list-group-item rounded border-0 py-0 my-0 px-3">
                        <div className="d-flex align-items-center justify-content-between">
                            <p className="text-decoration-none text-muted">
                                <span className="text-primary align-middle  fw-bolder me-2">
                                     <i className="bi bi-play-circle"/>
                                </span>
                                <span className="align-middle" style={{fontSize:"12px"}}>{video.title}</span>
                            </p>
                            <p className="text-muted me-2">
                                <span className="align-middle"> {secondsMs(video.duration)}</span>
                            </p>
                        </div>
                    </div>

                </div>
            ))
            }
        </div>
    );

};

export default CourseDetailsSectionItem;