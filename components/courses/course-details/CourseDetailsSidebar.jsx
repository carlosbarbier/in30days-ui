import React from 'react';
import {Divider} from "antd";
import Router from 'next/router'
import {storeToLocalstorage} from "../../../utils";
const CourseDetailsSidebar = ({course}) => {
    const handleBuyCourse = (e) => {
        storeToLocalstorage("cart",e.currentTarget.dataset.course)
        Router.push("/cart")
    }

    return (
        <div className="course-details-sidebar">

            <div className="sidebar-widget sidebar-preview border-0">
                <div className="preview-course-content">
                    <div className="preview-course__price d-flex align-items-center">
                        <div className="price-current">
                            £{course.price}
                        </div>
                    </div>
                </div>
                <div className="d-grid px-3">
                    <button className="btn btn-primary btn-sm mt-2" type="button"
                            onClick={handleBuyCourse}
                            data-course={JSON.stringify({
                                slug: course.slug,
                                title: course.title,
                                price: course.price
                            })}
                    >
                        Add to cart
                    </button>
                    <button className="btn btn-light btn-sm mt-2" type="button">
                        Preview
                    </button>
                </div>
            </div>
            <div className="sidebar-widget border-0">
                <div className="addons">
                    <h5 className="widget-title">What is included?</h5>
                    <Divider plain/>
                    <p><i className="bi bi-play-circle" style={{color: "#754FFE"}}/> 12 hours video
                    </p>
                    <Divider plain/>
                    <p><i className="bi bi-award-fill" style={{color: "#A8DEFC"}}/> Certificate</p>
                    <Divider plain/>
                    <p><i className="bi bi-wallet2" style={{color: "#5FD9C0"}}/> Lifetime access</p>
                    <Divider plain/>
                    <p><i className="bi bi-cash" style={{color: "#5FD9C0"}}/> 45 Days Money Back</p>
                    <Divider plain/>
                    <p><i className="bi bi-journal-code" style={{color: "#5FD9C0"}}/> 12 Projects
                    </p>
                    <Divider plain/>
                    <p><i className="bi bi-code-slash" style={{color: "#FFB27B"}}/> Sources Codes
                    </p>
                </div>
            </div>
        </div>
    );
};

export default CourseDetailsSidebar;