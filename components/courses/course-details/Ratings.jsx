import React from 'react';

const Ratings = () => {
    return (
        <div className="ratings-wrapper">
            <div className="d-flex border-bottom rating-box pb-2 mb-2 pt-0">
                <img className="rounded-circle source-profile" src="/profiles/profile-1.png" alt="image" />
                <div className=" ms-3">
                    <p className="mb-1"><span className="rating-author">Max Hawkins  </span> <span className="ms-1 text-muted rating-date">2 Days ago</span></p>
                    <div className="fs-6 mb-2">
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                    </div>
                    <p>Lectures were at a really good pace and I never felt lost. The instructor was well
                        informed and allowed me to learn and navigate Figma easily.</p>
                    <div className="d-lg-flex">
                        <p className="mb-0">Was this review helpful?</p>
                        <a href="#" className="btn btn-sm btn-light ms-lg-3">Yes</a>
                        <a href="#" className="btn btn-sm btn-outline-white ms-1">No</a>
                    </div>
                </div>
            </div>
            <div className="d-flex border-bottom rating-box pb-4 mb-4">
                <img className="rounded-circle source-profile" src="/profiles/profile-1.png" alt="image" />
                <div className=" ms-3">
                    <p className="mb-1"><span className="rating-author">Max Hawkins  </span><span className="ms-1 text-muted rating-date">2 Days ago</span>
                    </p>
                    <div className="fs-6 mb-2">
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                    </div>
                    <p>Lectures were at a really good pace and I never felt lost. The instructor was well
                        informed and allowed me to learn and navigate Figma easily.</p>
                    <div className="d-lg-flex">
                        <p className="mb-0">Was this review helpful?</p>
                        <a href="#" className="btn btn-sm btn-light ms-lg-3">Yes</a>
                        <a href="#" className="btn btn-sm btn-outline-white ms-1">No</a>
                    </div>
                </div>
            </div>
            <div className="d-flex border-bottom rating-box pb-4 mb-4">
                <img className="rounded-circle source-profile" src="/profiles/profile-1.png" alt="image" />
                <div className=" ms-3">
                    <h6 className="mb-1"><span className="rating-author">Max Hawkins  </span><span className="ms-1 text-muted rating-date">2 Days ago</span></h6>
                    <div className="fs-6 mb-2">
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                        <i className="bi bi-star text-warning"/>
                    </div>
                    <p>Lectures were at a really good pace and I never felt lost. The instructor was well
                        informed and allowed me to learn and navigate Figma easily.</p>
                    <div className="d-lg-flex">
                        <p className="mb-0">Was this review helpful?</p>
                        <a href="#" className="btn btn-sm btn-light ms-lg-3">Yes</a>
                        <a href="#" className="btn btn-sm btn-outline-white ms-1">No</a>
                    </div>
                </div>
            </div>
            <div className="text-center">
                <a className="btn btn-primary" href="#">More </a>
            </div>
        </div>
    );
};

export default Ratings;