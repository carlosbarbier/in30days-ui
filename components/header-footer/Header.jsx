import React from 'react';
import Link from "next/link";
import Image from "next/image";
import {getCart, getUser, isLogin} from "../../utils";
import UserDashboard from "./UserDashboard";
import {Role} from "../../constants";

const Header = () => {
    const user = getUser()
    const cart = getCart()
    return (
        <div id="header">
            <nav className="navbar navbar-expand-lg">
                <div className="container">
                    <div className="d-flex flex-grow-1">
                        <Link href="/">
                            <a>
                                <span className="w-100 d-lg-none d-block"/>
                                <Image src="/logo1.png" alt="logo" width="200" height="70"/>
                            </a>

                        </Link>

                    </div>
                    <div className="collapse navbar-collapse flex-grow-1 text-right ">
                        <ul className="navbar-nav ms-auto flex-nowrap">
                            <li className="nav-item">
                                <Link href="/course">
                                    <a className="nav-link m-2 menu-item fw-bold">Course</a>
                                </Link>

                            </li>
                            <li className="nav-item">
                                <Link href="/watch/Python-Course"
                                      as={`/watch/Python-Course`}>
                                    <a className="nav-link m-2 menu-item fw-bold">Watch</a>
                                </Link>
                            </li>

                            <li className="nav-item">
                                <Link href="/courses/Python-Course"
                                      as={`/courses/Python-Course`}>
                                    <a className="nav-link m-2 menu-item fw-bold">Python</a>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link href="/courses/Php-Course"
                                      as={`/courses/Php-Course`}>
                                    <a className="nav-link m-2 menu-item fw-bold">Kotlin</a>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link href="/courses/Php-Course"
                                      as={`/courses/Php-Course`}>
                                    <a className="nav-link m-2 menu-item fw-bold">Aws</a>
                                </Link>
                            </li>

                            <li className="nav-item">
                                <Link href="/courses/Php-Course"
                                      as={`/courses/Php-Course`}>
                                    <a className="nav-link m-2 menu-item fw-bold">Php</a>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link href="/details">
                                    <a href="#" className="nav-link m-2 menu-item fw-bold">Java</a>
                                </Link>

                            </li>
                        </ul>
                    </div>
                    <div className="collapse navbar-collapse">
                        <div className="ms-auto mt-3 mt-lg-0">
                            <Link href="/cart">
                                <div className="shopping_cart_icon ">
                                    <span className="total fw-bold h6">
                                    {cart ? "1" : ""}
                                    </span>
                                    <span>
                                        <i className="bi bi-bag" style={{fontSize: "18px"}}/>
                                    </span>
                                </div>
                            </Link>
                        </div>
                    </div>
                    {!isLogin() &&
                        <div className="collapse navbar-collapse authentication">
                            <div className="ms-auto mt-3 mt-lg-0">
                                <Link href="/login">
                                    <a className="btn btn-white shadow-sm me-1 fw-bold">Sign In</a>
                                </Link>

                                <Link href="/register">
                                    <a className="btn btn-primary fw-bold btn_register">Sign Up</a>
                                </Link>
                            </div>
                        </div>
                    }
                    {user && user.role === Role.USER && <UserDashboard url="/student/dashboard"/>}
                    {user && user.role === Role.TEACHER && <UserDashboard url="/teacher/home"/>}


                </div>
            </nav>
        </div>


    );
};

export default Header;