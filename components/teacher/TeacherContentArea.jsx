import React from 'react';

const TeacherContentArea = ({children}) => {
    return (
        <>
            {children}
        </>
    );
};

export default TeacherContentArea;