import React from 'react';
import {secondsMs} from "../../../utils";

const SectionItem = ({section}) => {
    return (
        <div className="list-group list-group-flush border-top-0">
            {section.videos && section.videos.map(video => (
                <div className="course mb-2" key={video.id}>
                    <div className="list-group-item rounded border-0 px-3">
                        <div className="d-flex align-items-center justify-content-between">

                            <p className="text-decoration-none text-muted">
                                                    <span className="text-primary align-middle  fw-bolder me-2">
                                                         <i className="bi bi-play-circle"/>
                                                    </span>
                                <span className="align-middle">{video.title}</span>
                            </p>
                            <p className="text-muted me-2">
                                <span className="align-middle"> {secondsMs(video.duration)}</span>
                            </p>
                        </div>

                    </div>

                </div>
            ))
            }
        </div>
    );
};

export default SectionItem;