import React from 'react';
import Link from "next/link"
const TeacherSidebar = () => {
    return (
        <nav className="navbar navbar-expand-md  mb-4   mt-0" >
                <div className="navbar-nav flex-column">
                    <span className="navbar-header fw-bolder ">Courses</span>
                    <ul className="list-unstyled ">
                        <li className="nav-item">
                            <Link href="/teacher/courses/new-course">
                                <a className="nav-link"> <i className="bi bi-calendar3-event  mr-2"/>{""} Create New Course</a>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link href="/teacher/courses">
                                <a className="nav-link"> <i className="bi bi-calendar3-event  mr-2"/>{""} All  Courses</a>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#"> <i className="bi bi-calendar3-event mr-2"/> Invoice</a>
                        </li>
                    </ul>

                    <span className="navbar-header fw-bolder">Account Settings</span>
                    <ul className="list-unstyled ms-n2 mb-0">
                        <li className="nav-item">
                            <a className="nav-link" href="#"><i className="bi bi-gear mr-2"/> {""}Edit Profile</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#"><i className="bi bi-file-lock mr-2"/>{""} Security</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#"><i className="bi bi-bell mr-2"/>{""} Notifications</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#"><i className="bi bi-file-earmark-lock mr-2"/>{""} Profile
                                Privacy</a>
                        </li>

                        <li className="nav-item">
                            <a className="nav-link" href="#"> <i className="bi bi-archive mr-2"/>{""} Delete
                                Profile</a>
                        </li>

                        <li className="nav-item">
                            <a className="nav-link " href="#"> <i className="bi bi-folder-symlink"/> {""} Linked
                                Accounts</a>
                        </li>

                        <li className="nav-item">
                            <a className="nav-link" href="#"><i className="bi bi-box-arrow-right"/> Sign Out</a>
                        </li>
                    </ul>
                </div>
        </nav>


    );
};

export default TeacherSidebar;