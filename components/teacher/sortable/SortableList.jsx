import React from 'react';
import SortableItem from "./SortableItem";
import {SortableContainer} from "react-sortable-hoc";

const SortableList = SortableContainer(({videos}) => {
    return (
        <div>
            {videos.map((video, index) => (
                <SortableItem key={video.id}
                              index={index}
                              title={(index + 1) + " - " + video.title}
                              id={video.id}
                              url={video.url}
                              position={video.position}/>
            ))}

        </div>
    );
});

export default SortableList;

