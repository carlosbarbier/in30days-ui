import React, {useState} from 'react';
import ErrorComponent from "../../../common/ErrosComponent";
import {useForm} from "react-hook-form";
import {yupResolver} from '@hookform/resolvers/yup/dist/yup.umd';
import {courseCreationSchema} from "../../../../validation";
import {Modal} from 'antd';
import {createNewCourse} from "../../../../api/teacher/course";
import {getErrors} from "../../../../utils";
import Router from "next/router";
import LoadingButton from "../../../common/LoadingButton";

const NewCourseComponent = () => {
    const [loading, setLoading] = useState(false)
    const [isDeleted, setIsDeleted] = useState(false)
    const [item, setItem] = useState('');
    const [tags, setTags] = useState([]);
    const [apiErrs, setApiErrors] = useState(null)
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        setIsModalVisible(true);
    };
    const handleOk = () => {
        if (item !== "") {
            let arr = tags
            arr.push(item)
            setTags(arr)
        }
        setIsModalVisible(false);
        setItem("")
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };
    const {register, handleSubmit, reset, formState: {errors}} = useForm({resolver: yupResolver(courseCreationSchema)});

    const onSubmit = async (data) => {
        setLoading(true)
        const formData = new FormData();
        formData.append("image", data.image[0]);
        formData.append('description', data.description);
        formData.append('title', data.title);
        formData.append('price', data.price);
        formData.append('programming_language', data.programming_language);
        formData.append('things_to_learn', tags.toString());
        const response = await createNewCourse(formData)
        if (response.status !== 201) {
            const err = await getErrors(response)
            setApiErrors(err)
        } else {
            reset({title: "", price: "", programming_language: "", image: "", description: ""})
            setTags([])
            Router.push("/teacher/courses")
        }

    };

    const handleItem = (e) => {
        setItem(e.target.value)

    }

    const handleRemoveItem = (e) => {
        setIsDeleted(false)
        const index = e.target.getAttribute("data-index")
        let arr = tags
        if (index > -1) {
            arr.splice(index, 1);
        }
        setTags(arr)
        if (!isDeleted) {
            setIsDeleted(true)
        }

    }

    return (
        <div className="card bg-white border-0 rounded-3 new-course-content p-5" id="teacher-new-course">
            <form onSubmit={handleSubmit(onSubmit)}>
                {apiErrs && <ErrorComponent errors={apiErrs}/>}
                <div className="mb-2">
                    <label htmlFor="title" className="form-label">Title</label>
                    <input type="text"
                           className="form-control"  {...register("title")}/>
                    <span className="text-danger">{errors.title?.message}</span>
                </div>
                <div className="mb-2">
                    <label htmlFor="programming_language" className="form-label">Programming Language</label>
                    <input type="text"
                           className="form-control"  {...register("programming_language")}/>
                    <span className="text-danger">{errors.programming_language?.message}</span>
                </div>
                <div className="mb-2">
                    <label htmlFor="price" className="form-label">Price</label>
                    <input type="text"
                           className="form-control"  {...register("price")}/>
                    <span className="text-danger">{errors.price?.message}</span>
                </div>
                <div className="mb-2">
                    <label htmlFor="price" className="form-label">Item to learn</label>
                    <button type="button" className="btn btn-outline-light w-100 btn-hover"
                            onClick={showModal}>{"Add"}</button>
                    <Modal title="Add Item To Learn" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                        <input name="item" onChange={handleItem} value={item} className="form-control mt-2"/>
                    </Modal>
                    {tags && tags.length > 0 &&
                        <div className="mx-4 mt-3">
                            {tags.map((tag, i) => (
                                    <div className="d-flex justify-content-between" key={i}>
                                        <li>{tag}</li>
                                        <span className="remove-btn fst-italic" data-index={i}
                                              onClick={handleRemoveItem}>Remove</span>
                                    </div>
                                )
                            )}
                            <hr/>
                        </div>
                    }

                </div>
                <div className="mb-2">
                    <label htmlFor="description" className="form-label">Description</label>
                    <textarea className="form-control" rows="5" {...register("description")}/>
                    <span className="text-danger">{errors.description?.message}</span>
                </div>
                <div className="mb-4">
                    <label htmlFor="image" className="form-label">Image</label>
                    <input className="form-control" type="file"  {...register("image")} />
                    <span className="text-danger">{errors.image?.message}</span>
                </div>
                <LoadingButton loading={loading} css_class="btn btn-secondary" message="Create a course" load_message="Creating course"/>
            </form>


        </div>
    );
};

export default NewCourseComponent;