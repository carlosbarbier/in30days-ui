import React from 'react';
import moment from "moment";
import Link from 'next/link'
const CourseItem = ({payload}) => {
    return (
        <div className="d-flex justify-content-between course-item bg-white mb-3 pe-5">
            <div className="d-flex flex-row align-items-center ">
                <div>
                    <img
                        src={`${payload.course.url}`}
                        className="img-fluid rounded-3" alt="Image"  style={{width:"100px",height:"100px"}}/>
                </div>
                <div className="ms-3">
                    <h6>{payload.course.title} </h6>
                    <p className="small mb-0">Last updated : <span>{moment(payload.course.updated_at).fromNow()}</span></p>
                </div>
            </div>
            <div className="d-flex flex-row align-items-center">
                <div className="me-4">
                    <Link href="/teacher/courses/[slug]/edit"
                          as={`/teacher/courses/${payload.course.slug}/edit`}>
                        <a href="" className="btn btn-blue-1 btn-sm fw-bold">
                            <span><i className="bi bi-pencil-square"/></span>
                        </a>
                    </Link>
                </div>
                <div>
                    <Link href="/teacher/courses/[slug]/preview"
                          as={`/teacher/courses/${payload.course.slug}/preview`}>
                        <a href="" className="btn btn-blue-1 btn-sm fw-bold">
                            <span> <i className="bi bi-eye"/></span>
                        </a>
                    </Link>


                </div>
            </div>
        </div>
    );
};

export default CourseItem;