import React from 'react';
import CourseItem from "./CourseItem";

const AllCourses = ({data}) => {
    return (
        <section id="teacher-all-course" className="min-vh-100">
            {data && data.map((payload, index) => (
                <CourseItem key={index} payload={payload}/>
            ))}
        </section>
    );
};


export default AllCourses;