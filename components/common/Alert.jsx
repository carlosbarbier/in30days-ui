import React from 'react';

const Alert = ({type,message}) => {
    return (
        <div className={`alert ${type} d-flex justify-content-center`} role="alert">
                {message}
        </div>
    );
};

export default Alert;