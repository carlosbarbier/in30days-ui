import React from 'react';
import {Breadcrumb} from "antd";
import Link from 'next/link'
const BreadcrumbComponent = ({path}) => {
    const items = path.split("/")
    return (
        <div className="border-bottom pb-3 ">
            <Breadcrumb>
                {items && items.filter(item=>(item!==""))
                    .map((item,i) => (
                    <Breadcrumb.Item key={i}>
                        <Link href={`/${item}`}>
                            <a>{item}</a>
                        </Link>

                    </Breadcrumb.Item>
                ))}
            </Breadcrumb>
        </div>

    );
};

export default BreadcrumbComponent;