import SocialLogin from "react-social-login";
import { Component } from "react";
class SocialAuth extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return <div onClick={this.props.triggerLogin}>{this.props.children}</div>;
    }
}

export default SocialLogin(SocialAuth);