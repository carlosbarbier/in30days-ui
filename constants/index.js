export const StatusEnum = Object.freeze({
    CREATED: 'created',
    ACCEPTED: 'accepted',
    REJECTED: 'rejected',
    COMPLETED: 'completed',
})

export const Role = Object.freeze({
    TEACHER: 'teacher',
    USER: 'user',
})




