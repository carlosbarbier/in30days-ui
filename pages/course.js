import React from 'react';
import MainLayout from "../components/layouts/MainLayout";
import {Tabs} from "antd";
import CourseDetailsHeader from "../components/courses/course-details/CourseDetailsHeader";
import Curriculum from "../components/courses/course-details/Curriculum";
import ThingsToLearn from "../components/courses/course-details/ThingsToLearn";
import DetailsReview from "../components/courses/course-details/DetailsReview";
import Faq from "../components/courses/course-details/Faq";
import CourseDetailsSidebar from "../components/courses/course-details/CourseDetailsSidebar";

const Course = () => {
    const {TabPane} = Tabs;

    function callback(key) {
        console.log(key);
    }

    function executeOnClick(key) {
        console.log(key);
    }

    return (
        <MainLayout>
            <div className="container">
                <CourseDetailsHeader/>
                <section className="course-detail">

                        <div className="row">
                            <div className="col-lg-8 col-sm-12 col-12 col-md-12">
                                <Tabs defaultActiveKey="1" onChange={callback} >
                                    <TabPane tab="Content" key="1">
                                        <Curriculum videos={[]}/>
                                    </TabPane>
                                    <TabPane tab="Description" key="2">
                                        <ThingsToLearn  things_to_learn={ []} description={""}/>
                                    </TabPane>
                                    <TabPane tab="Reviews" key="3">
                                        <DetailsReview/>
                                    </TabPane>
                                    <TabPane tab="Faq" key="4">
                                        <Faq/>
                                    </TabPane>
                                </Tabs>
                            </div>
                            <div className="col-lg-4 col-12 col-md-12">
                                <CourseDetailsSidebar price={24}/>
                            </div>

                        </div>
                </section>

            </div>
        </MainLayout>
    );
};

export default Course;