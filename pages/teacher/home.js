import React from 'react';
import TeacherLayout from "../../components/layouts/TeacherLayout";
import TeacherHomeComponent from "../../components/teacher/TeacherHomeComponent";


const TeacherHome = () => {
    return (
        <TeacherLayout>
            <TeacherHomeComponent/>
        </TeacherLayout>
    );
};

export default TeacherHome;