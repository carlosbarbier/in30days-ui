import React, {useEffect, useState} from 'react';
import TeacherLayout from "../../../../components/layouts/TeacherLayout";
import {getCourseBySlug} from "../../../../api/teacher/course";
import {Modal} from 'antd';
import {yupResolver} from '@hookform/resolvers/yup/dist/yup.umd';
import {useForm} from "react-hook-form";
import {lectureSchema} from "../../../../validation";
import {getErrors, secondsMs} from "../../../../utils";
import LoadingButton from "../../../../components/common/LoadingButton";
import {createNewVideo} from "../../../../api/teacher/video";
import {createNewSection} from "../../../../api/teacher/section";
import { Collapse } from 'antd';
import SectionItem from "../../../../components/teacher/section/SectionItem";


export const getServerSideProps = ({params}) => {
    const {slug} = params;
    return {
        props: {slug},
    };
};

const Edit = ({slug}) => {
    const { Panel } = Collapse;
    const [course, setCourse] = useState(null)
    const [wording, setWording] = useState(null)
    const [loading, setLoading] = useState(false)
    const [success, setSuccess] = useState(null)
    const [sectionId, setSectionId] = useState(null)
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [apiErrs, setApiErrors] = useState(null)

    useEffect(() => {
        getCourseBySlug(slug)
            .then(response => response.json())
            .then((data) => setCourse(data))
    }, [success]);

    const {register, handleSubmit, reset, formState: {errors}} = useForm({resolver: yupResolver(lectureSchema)});

    const onSubmit = async (data) => {
        setLoading(true)
        const formData = new FormData();
        formData.append("video", data.video[0]);
        formData.append("title", data.title);
        formData.append('section_id', sectionId)
        const response = await createNewVideo(formData)
        if (response.status !== 201) {
            const err = await getErrors(response)
            setApiErrors(err)
            setLoading(false)
        } else {
            reset({title: "", video: ""})
            setIsModalVisible(false);
            setLoading(false)
            setSectionId(null)
            setSuccess(Date.now())
        }
    };

    const handleChange = (e) => {
        setWording(e.target.value);
    };

    const handleAddLecture = (e) => {
        setIsModalVisible(true);
        const id = e.target.getAttribute("data-section-id")
        setSectionId(id)
    }

    const handleCancel = () => {
        setIsModalVisible(false);
        setSectionId(null)
        reset({title: "", video: ""})
    };

    const handleAfterClose = () => {
        reset({title: "", video: ""})
        setSectionId(null)

    }

    const handleSubmitSection = async (e) => {
        setLoading(true)
        e.preventDefault();
        const formData = new FormData();
        formData.append("wording", wording);
        const response = await createNewSection(slug,formData)
        if (response.status !== 201) {
            const err = await getErrors(response)
            setLoading(false)
            console.log(err)
        } else {
            setWording("")
            setLoading(false)
            setSuccess(Date.now())
        }

    }

    return (
        <TeacherLayout>
            <div className="bg-white p-5" id="teacher-edit">
                <div className="add-new-section className bg-blue-1 px-4 py-2 mb-3">
                    <form onSubmit={handleSubmitSection}>
                        <div className="mb-2">
                            <input type="text" className="form-control" name="wording"
                                   onChange={handleChange} value={wording}/>
                        </div>
                        <div className="d-flex justify-content-end">
                            <LoadingButton loading={loading}
                                           css_class="btn btn-add  btn-primary btn-sm mt-1"
                                           message="Add new Section +"
                                           load_message="Saving"/>
                        </div>

                    </form>
                </div>
                {course && course.sections && course.sections.map(section => (
                    <Collapse bordered={false}  ghost={true } key={section.id} expandIconPosition="right" className="rounded bg-blue-1 px-4 pt-2 pb-3 mb-4 ">
                        <Panel header={section.wording} >
                            <div className=" course-section" key={section.id}>
                               <SectionItem section={section}/>
                                <Modal footer={null} destroyOnClose={true} afterClose={handleAfterClose} closable={false}
                                       visible={isModalVisible}>
                                    <form onSubmit={handleSubmit(onSubmit)}>
                                        <div className="mb-2">
                                            <label htmlFor="title" className="form-label">Title</label>
                                            <input type="text"
                                                   className="form-control"  {...register("title")}/>
                                            <span className="text-danger">{errors.title?.message}</span>
                                        </div>
                                        <div className="mb-4">
                                            <label htmlFor="video" className="form-label">Video</label>
                                            <input className="form-control" type="file"  {...register("video")} />
                                            <span className="text-danger">{errors.video?.message}</span>
                                        </div>
                                        <div className="d-flex justify-content-between">
                                            <LoadingButton loading={loading}
                                                           css_class="btn btn-outline-primary btn-sm mt-3"
                                                           message="Save +"
                                                           load_message="Saving"/>
                                            <button type="button" className="btn btn-add  btn-outline-secondary btn-sm mt-3"
                                                    onClick={handleCancel}>Cancel+
                                            </button>

                                        </div>

                                    </form>

                                </Modal>
                                <span className="btn  btn-outline-primary btn-sm mt-3" data-section-id={section.id}
                                      onClick={handleAddLecture}>Add Lecture +</span>

                            </div>
                        </Panel>
                    </Collapse>

                ))}

            </div>
        </TeacherLayout>
    );
};

export default Edit;