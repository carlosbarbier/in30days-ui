import React, {useEffect, useState} from 'react';
import TeacherLayout from "../../../../components/layouts/TeacherLayout";
import {Collapse} from "antd";
import {getCourseBySlug} from "../../../../api/teacher/course";
import SectionItem from "../../../../components/teacher/section/SectionItem";
import {SortableContainer, SortableElement} from "react-sortable-hoc";

export const getServerSideProps = ({params}) => {
    const {slug} = params;
    return {
        props: {slug},
    };
};
const {Panel} = Collapse;
const SortableItem = SortableElement(({section}) => <SectionItem section={section}/>);
const SortableList = SortableContainer(({sections}) => {
    console.log(sections)
    return (
        <>
            <>
                {sections.map((section, index) => (
                    <SortableItem key={section.id} index={index} value={section.title} />
                ))}
            </>
            {/*{ sections.map(section => (*/}
            {/*    <Collapse bordered={false} ghost={true} key={section.id} expandIconPosition="right"*/}
            {/*              className="rounded bg-blue-1  pt-2 pb-3 mb-4 ">*/}
            {/*        <Panel header={section.wording}>*/}
            {/*            /!*<div className=" course-section" key={section.id}>*!/*/}
            {/*            /!*    <SectionItem section={section}/>*!/*/}
            {/*            /!*</div>*!/*/}
            {/*        </Panel>*/}
            {/*    </Collapse>*/}
            {/*))}*/}

        </>
    );
});

const Preview = ({slug}) => {
    const [course, setCourse] = useState(null)
    const [loading, setLoading] = useState(false)
    const [success, setSuccess] = useState(null)
    const [sectionId, setSectionId] = useState(null)
    const [apiErrs, setApiErrors] = useState(null)
    const items= ['Item 1', 'Item 2', 'Item 3', 'Item 4', 'Item 5', 'Item 6']

    useEffect(() => {
        getCourseBySlug(slug)
            .then(response => response.json())
            .then((data) => setCourse(data))
    }, []);
    const onSortEnd = async ({oldIndex, newIndex}) => {

    };
    return (
        <TeacherLayout title={course && course.title + " Details"}>
            <div className="bg-white ms-3  pt-4 pe-4">
                <div className="row">
                    <div className="col-7">
                        <div className="ms-3" id="teacher-preview">
                           <SortableList sections={course && course.sections} onSortEnd={onSortEnd}/>
                        </div>
                    </div>
                    <div className="col-5 bg-info mb-4">
                        <div className="">
                            <div className="pt-3">
                                <h5 className="">Course Price : {course && course.price}€ </h5>
                            </div>
                            <div className="pt-3">
                                <h5 className=" ">Course Description</h5>
                                <p>{course && course.description}</p>
                            </div>
                            <div className="pt-3">
                                <h5 className=" ">Things to learn</h5>
                                {course && course.things_to_learn.map((item, index) => (
                                    <li key={index}>{item}</li>
                                ))}

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </TeacherLayout>
    );
};

export default Preview;