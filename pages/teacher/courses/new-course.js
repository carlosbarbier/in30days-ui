import React from 'react';
import TeacherLayout from "../../../components/layouts/TeacherLayout";
import NewCourseComponent from "../../../components/teacher/course/new/NewCourseComponent";

const NewCourse = () => {
    return (
        <TeacherLayout>
            <NewCourseComponent/>
        </TeacherLayout>
    );
};

export default NewCourse;