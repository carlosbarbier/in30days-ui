import React from 'react';
import TeacherLayout from "../../../components/layouts/TeacherLayout";
import AllCourses from "../../../components/teacher/course/all/AllCourses";
import axios from "axios";
import cookies from "next-cookies";
import {getMyCourses} from "../../../api/teacher/course";

export const getServerSideProps = async (ctx) => {
    const {token} = cookies(ctx);
    const response = await getMyCourses(token);
    const data=await response.json()
    return {
        props: {data},
    };
};
const Courses = ({data}) => {
    return (
        <TeacherLayout>
            <AllCourses data={data}/>
        </TeacherLayout>
    );
};

export default Courses;