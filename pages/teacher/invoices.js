import React from 'react';
import StudentLayout from "../../components/layouts/StudentLayout";
import InvoiceComponent from "../../components/student/InvoiceComponent";


const Invoices = () => {
    return (
        <StudentLayout>
            <InvoiceComponent/>
        </StudentLayout>
    );
};

export default Invoices;