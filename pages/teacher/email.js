import React from 'react';
import EmailComponent from "../../components/student/EmailComponent";
import StudentLayout from "../../components/layouts/StudentLayout";



const Email = () => {
    return (
        <StudentLayout>
            <EmailComponent/>
        </StudentLayout>
    );
};

export default Email;