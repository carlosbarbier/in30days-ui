import React from 'react';
import StudentLayout from "../../components/layouts/StudentLayout";
import DashboardComponent from "../../components/student/DashboardComponent";
import {getCookie} from "../../utils";
import {getMyCourses} from "../../api/student/course";


const Dashboard = ({courses}) => {
    console.log(courses)
    return (
        <StudentLayout>
            <DashboardComponent courses={courses}/>
        </StudentLayout>
    );
};

Dashboard.getInitialProps = async (ctx) => {
    const token = getCookie('token', ctx.req);
    let courses = null;
    if (token) {
        try {
            const response = await getMyCourses(token)

            courses = await response.data
        } catch (error) {
            if (error.response.status !== 200) {
                courses = null;
            }
        }
    }
    if (courses === null) {
        ctx.res.writeHead(302, {Location: '/'});
        ctx.res.end();
    } else {
        return {courses:courses};

    }

};

export default Dashboard;