import React, {useEffect, useState} from 'react';
import MainLayout from "../components/layouts/MainLayout";
import {getErrors, getFromLocalstorage, getUser, removeFromLocalstorage} from "../utils";
import Router from 'next/router'
import {useForm} from "react-hook-form";
import {cartSchema} from "../validation";
import {yupResolver} from '@hookform/resolvers/yup/dist/yup.umd';
import ErrorComponent from "../components/common/ErrosComponent";
import {enrollStudentForPaidCourse} from "../api/cart";
import LoadingButton from "../components/common/LoadingButton";

const Cart = () => {
    const [course, setCourse] = useState(null)
    const [loading, setLoading] = useState(false)
    const [user,] = useState(getUser())
    const [apiErrs, setApiErrors] = useState(null)
    const [isDeleted, setIsDeleted] = useState(null)
    useEffect(() => {
        const item = getFromLocalstorage('cart')
        if (item) {
            setCourse(JSON.parse(item))
        }
    }, [isDeleted])

    const {register, handleSubmit, reset, formState: {errors}} = useForm({resolver: yupResolver(cartSchema)});

    const onSubmit = async (body) => {
        if (!user) {
            Router.replace('/login')
        } else {
            setLoading(true)
            const exp_month = body.expiry_date.slice(0, 2)
            const exp_year = "20" + body.expiry_date.slice(-2)
            const response = await enrollStudentForPaidCourse({
                card_number: body.card_number,
                cvc: body.cvc,
                exp_month,
                exp_year
            }, course.slug);

            if (response.status !== 200) {
                const err = await getErrors(response)
                setApiErrors(err)
                setLoading(false)
            } else {
                setLoading(false)
                removeFromLocalstorage('cart')
                Router.replace(`watch/${course.slug}`)

            }
        }

    };

    const handleRemove = () => {
        removeFromLocalstorage("cart")
        setIsDeleted(Date.now())
        setCourse(null)
    }

    return (
        <MainLayout>
            <div className="container">
                <div className="border-0 mt-3 py-5 bg-custom-1 mb-5 min-vh-100" id="cart-container">
                    {course &&
                        <>
                            <div className="shopping-list-wrapper">
                                <div className="row">
                                    <div className="col-lg-5 mx-auto">
                                        {course &&
                                            <div className="card card-body mb-4 border-0">
                                                <div className="d-flex justify-content-between">
                                                    <p>{course && course.title}</p>
                                                    <div>
                                                <span
                                                    className="font-weight-bold">Price: £{course && course.price}</span>
                                                        <button className="btn btn-sm btn-default"
                                                                style={{
                                                                    fontSize: "12px",
                                                                    padding: "5px",
                                                                    marginLeft: "22px"
                                                                }}
                                                                onClick={handleRemove}>remove
                                                        </button>
                                                    </div>

                                                </div>
                                            </div>
                                        }

                                    </div>
                                </div>
                            </div>
                            <div className="credit-card-wrapper mt-4">
                                <div className="row">
                                    <div className="col-lg-5 mx-auto">
                                        <p className="h5">Enter your Payments Details</p>
                                        {apiErrs && <ErrorComponent errors={apiErrs}/>}
                                        <div className="card pt-3 px-3 bg-white border-0 rounded-3">
                                            <form onSubmit={handleSubmit(onSubmit)}>
                                                <div className="mb-3">
                                                    <label htmlFor="cart_number" className="form-label">Card
                                                        Number</label>
                                                    <input type="text"
                                                           className="form-control"  {...register("card_number")}/>
                                                    <span className="text-danger">{errors.card_number?.message}</span>
                                                </div>
                                                <div className="row my-5">
                                                    <div className="col-md-6">
                                                        <span className="font-weight-normal card-text">Expiry Date(mm/yy)</span>
                                                        <div className="input">
                                                            <input type="text"
                                                                   className="form-control" {...register("expiry_date")}/>
                                                            <span
                                                                className="text-danger">{errors.expiry_date?.message}</span>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-6 "><span
                                                        className="font-weight-normal card-text">CVC/CVV</span>
                                                        <div className="input">
                                                            <input type="text"
                                                                   className="form-control" {...register("cvc")}/>
                                                            <span className="text-danger">{errors.cvc?.message}</span>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div className="d-grid py-3">
                                                    <LoadingButton loading={loading} css_class="btn btn-secondary"
                                                                   message={`Pay Now
                                                        £ ${course.price}`} load_message="Processing Payment"/>
                                                </div>

                                            </form>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </>
                    }
                    {!course &&
                        <div className="d-flex justify-content-center">
                            <p className="font-weight-bold h2 mt-5">Empty shopping cart</p>
                        </div>

                    }
                </div>
            </div>
        </MainLayout>
    )
}

export default Cart;