import React, {useState} from 'react';
import MainLayout from "../components/layouts/MainLayout";
import {useForm} from "react-hook-form";
import {userLoginSchema} from "../validation";
import {signInUser} from "../api/auth";
import {getErrors, isAuthenticated} from "../utils";
import Router from "next/router";
import ErrorComponent from "../components/common/ErrosComponent";
import {yupResolver} from '@hookform/resolvers/yup/dist/yup.umd';
import {Role} from "../constants";

const Login = () => {
    const [apiErrs, setApiErrors] = useState(null)
    const {register, handleSubmit, reset, formState: {errors}} = useForm({resolver: yupResolver(userLoginSchema)});
    const onSubmit = async (data) => {
        const response = await signInUser(data);
        if (response.status !== 200) {
            const err = await getErrors(response)
            setApiErrors(err)
        } else {
            const payload = await response.json()
            isAuthenticated(payload, () => {
                reset({password: "", email: ""})
                if (payload.user.role === Role.TEACHER) {
                    Router.push("/teacher/home")
                } else if (payload.user.role === Role.USER) {
                    Router.push("/student/dashboard")
                }
            })
        }

    };
    return (
        <MainLayout>
            <div className="container d-flex flex-column mt-5">
                <div className="row align-items-center justify-content-center g-0 ">
                    <div className=" col-md-4">
                        <div className="card shadow border-0 ">
                            <div className="card-body p-6">
                                <div className="mb-4 text-center ">
                                    <h3 className="mb-3 fw-bold ">Sign in</h3>
                                </div>
                                <form onSubmit={handleSubmit(onSubmit)}>
                                    {apiErrs && <ErrorComponent errors={apiErrs}/>}
                                    <div className="mb-3">
                                        <label htmlFor="email" className="form-label">Email</label>
                                        <input type="email"
                                               className="form-control"  {...register("email")}/>
                                        <span className="text-danger">{errors.email?.message}</span>
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="password" className="form-label">Password</label>
                                        <input type="password"
                                               className="form-control"  {...register("password")}/>
                                        <span className="text-danger">{errors.password?.message}</span>
                                    </div>
                                    <div className="d-lg-flex justify-content-between align-items-center mb-4">
                                        <div>
                                            <a href="#" className="text-black" style={{fontSize: "12px"}}>Forgot your
                                                password?</a>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="d-grid py-3">
                                            <button type="submit" className="btn btn-secondary ">Sign in</button>
                                        </div>
                                    </div>
                                    <hr className="my-4"/>
                                    <div className="mt-4 text-center">
                                        <a href="#" className="btn-social btn-social-outline btn-facebook mx-1">
                                            <i className="bi bi-facebook"/>
                                        </a>

                                        <a href="#" className="btn-social btn-social-outline btn-twitter mx-1">
                                            <i className="bi bi-twitter"/>
                                        </a>

                                        <a href="#" className="btn-social btn-social-outline btn-linkedin mx-1">
                                            <i className="bi bi-linkedin"/>
                                        </a>
                                        <a href="#" className="btn-social btn-social-outline btn-github mx-1">
                                            <i className="bi bi-github"/>
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </MainLayout>
    )
}

export default Login;