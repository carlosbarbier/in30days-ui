import React from 'react';
import VideoComponent from "../components/videos/VideoComponent";
import MainLayout from "../components/layouts/MainLayout";
import CommentsSection from "../components/videos/CommentsSection";


const Watch = () => {
    return (
        <MainLayout>
            <VideoComponent/>
            <CommentsSection/>
        </MainLayout>
    )
}

export default Watch;