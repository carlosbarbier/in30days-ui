import Layout from "../components/layouts/MainLayout";


export default function Custom404() {
    return <Layout>
        <div className="d-flex justify-content-center align-items-center" style={{height: "70vh"}}>
            <h4 className="mx-auto">Oops Page not found</h4>
        </div>
    </Layout>

}
