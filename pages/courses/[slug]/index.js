import React from 'react';
import MainLayout from "../../../components/layouts/MainLayout";
import CourseDetailsHeader from "../../../components/courses/course-details/CourseDetailsHeader";
import {Tabs} from "antd";
import Curriculum from "../../../components/courses/course-details/Curriculum";
import ThingsToLearn from "../../../components/courses/course-details/ThingsToLearn";
import Faq from "../../../components/courses/course-details/Faq";
import CourseDetailsSidebar from "../../../components/courses/course-details/CourseDetailsSidebar";
import {getCourse} from "../../../api/course";

const {TabPane} = Tabs;


const CourseDetails = ({course}) => {

    function callback(key) {
        console.log(key);
    }

    return (
        <MainLayout>
            <div className="container">
                <CourseDetailsHeader title={course.title} total_student={course.total_student} last_updated={course.updated_at} author={course.author}/>
                <section className="course-detail pb-5">
                    <div className="row">
                        <div className="col-lg-8 col-sm-12 col-12 col-md-12">
                            <Tabs defaultActiveKey="1" onChange={callback}>
                                <TabPane tab="Content" key="1">
                                    <div id="course-details-content">
                                        <Curriculum sections={course.sections}/>
                                    </div>
                                </TabPane>
                                <TabPane tab="Description" key="2">
                                    <ThingsToLearn things_to_learn={course.things_to_learn} description={course.description}/>
                                </TabPane>
                                <TabPane tab="Faq" key="4">
                                    <Faq/>
                                </TabPane>
                            </Tabs>
                        </div>
                        <div className="col-lg-4 col-12 col-md-12">
                            <CourseDetailsSidebar course={course}/>
                        </div>
                    </div>
                </section>

            </div>
        </MainLayout>
    );
};

CourseDetails.getInitialProps = async ({query}) => {
    const course = await getCourse(query.slug)
    return {course:course};
};
export default CourseDetails;