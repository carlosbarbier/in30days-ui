import React, {useState} from 'react';
import {useForm} from "react-hook-form";
import Router from "next/router";
import {yupResolver} from '@hookform/resolvers/yup/dist/yup.umd';
import MainLayout from "../components/layouts/MainLayout";

import {userRegisterSchema} from "../validation";
import {registerUser} from "../api/auth";
import {getErrors} from "../utils";
import ErrorComponent from "../components/common/ErrosComponent";

const Register = () => {
    const [loading, setLoading] = useState(false)
    const [apiErrs, setApiErrors] = useState(null)
    const {register, handleSubmit, reset ,formState: {errors}} = useForm({resolver: yupResolver(userRegisterSchema)});
    const onSubmit = async (user) => {
        const response = await registerUser(user);
        if (response.status !== 201) {
            const err=await getErrors(response)
            setApiErrors( err)
        }else{
            reset({name: "", password: "", email: ""})
            Router.push("/student/dashboard")
        }

    };
    return (
        <MainLayout>
            <div className="container d-flex flex-column my-5">
                <div className="row align-items-center justify-content-center g-0 ">
                    <div className=" col-md-4">
                        <div className="card shadow border-0 ">
                            <div className="card-body p-6">

                                <div className="mb-4 text-center ">
                                    <h3 className="mb-3 fw-bold ">Register</h3>
                                </div>
                                <form onSubmit={handleSubmit(onSubmit)}>
                                    {apiErrs && <ErrorComponent errors={apiErrs}/>}
                                    <div className="mb-3">
                                        <label htmlFor="name" className="form-label">Name</label>
                                        <input type="text"
                                               className="form-control"  {...register("name")}/>
                                        <span className="text-danger">{errors.name?.message}</span>
                                    </div>

                                    <div className="mb-3">
                                        <label htmlFor="email" className="form-label">Email</label>
                                        <input type="email"
                                               className="form-control" {...register("email")} />
                                        <span className="text-danger">{errors.email?.message}</span>
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="password" className="form-label">Password</label>
                                        <input type="password" className="form-control"
                                               name="password" {...register("password")}  />
                                        <span className="text-danger">{errors.password?.message}</span>
                                    </div>
                                    <div>
                                        <div className="d-grid ">
                                            <button type="submit" className="btn btn-secondary ">Sign Up</button>
                                        </div>
                                    </div>
                                    <hr className="my-4"/>
                                    <div className="mt-4 text-center">
                                        <a href="#" className="btn-social btn-social-outline btn-facebook mx-1">
                                            <i className="bi bi-facebook"/>
                                        </a>

                                        <a href="#" className="btn-social btn-social-outline btn-twitter mx-1">
                                            <i className="bi bi-twitter"/>
                                        </a>

                                        <a href="#" className="btn-social btn-social-outline btn-linkedin mx-1">
                                            <i className="bi bi-linkedin"/>
                                        </a>
                                        <a href="#" className="btn-social btn-social-outline btn-github mx-1">
                                            <i className="bi bi-github"/>
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </MainLayout>
    )
}

export default Register;