import MainLayout from "../../components/layouts/MainLayout";
import VideoComponent from "../../components/videos/VideoComponent";
import {getCookie} from "../../utils";
import {getCourseToWatch} from "../../api/student/course";

const Watch = ({course}) => {
    return (
        <MainLayout>
            <div id="watch-details-page" className="mt-2">
                <VideoComponent sections={course.sections} ids={course.video_ids} slug={course.slug}
                                defaultUrl={course.sections[0].videos[0].url}/>
            </div>

        </MainLayout>
    )
}
Watch.getInitialProps = async (ctx) => {
    const token = getCookie('token', ctx.req);
    let courseData = null;
    if (token) {
        try {
            const response = await getCourseToWatch(ctx.query.slug, token)
            courseData = response.data
        } catch (error) {
            if (error.response.status !== 200) {
                courseData = null;
            }
        }
    }
    if (courseData === null) {
        ctx.res.writeHead(302, {Location: '/'});
        ctx.res.end();
    } else {
        return {course: courseData};

    }

};
export default Watch;