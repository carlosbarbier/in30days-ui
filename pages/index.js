import styles from '../styles/Home.module.css'
import MainLayout from "../components/layouts/MainLayout";

export default function Home() {
    return (
        <MainLayout>
            <main className={styles.main}>
                <h1 className={styles.title}>
                    Welcome to <span className="text-primary">In30Days!</span>
                </h1>

                <div className={styles.grid}>
                    <a href="#" className={styles.card}>
                        <h2>Learn Python &rarr;</h2>
                        <p>Find in-depth information about Next.js features and API.</p>
                    </a>

                    <a href="#" className={styles.card} >
                        <h2>Learn Java &rarr;</h2>
                        <p>Learn about Next.js in an interactive course with quizzes!</p>
                    </a>

                    <a
                        href="#"
                        className={styles.card}
                    >
                        <h2>Learn Kotlin &rarr;</h2>
                        <p>Discover and deploy boilerplate example Next.js projects.</p>
                    </a>

                    <a
                        href="#"
                        className={styles.card}
                    >
                        <h2>Learn Javascript &rarr;</h2>
                        <p>
                            Instantly deploy your Next.js site to a public URL with Vercel.
                        </p>
                    </a>
                </div>
            </main>
        </MainLayout>
    )
}
