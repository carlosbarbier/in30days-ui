import React from 'react';

import {Divider, Tabs} from 'antd';
import MainLayout from "../components/layouts/MainLayout";
import Curriculum from "../components/courses/course-details/Curriculum";
import ThingsToLearn from "../components/courses/course-details/ThingsToLearn";
import DetailsReview from "../components/courses/course-details/DetailsReview";
import Faq from "../components/courses/course-details/Faq";
import CourseDetailsSidebar from "../components/courses/course-details/CourseDetailsSidebar";

const Details = ({data}) => {

    const {TabPane} = Tabs;

    function callback(key) {
        console.log(key);
    }
    function executeOnClick(key) {
        console.log(key);
    }
    return (
        <MainLayout>
            <div id="course-details-page">
                <section className="details-wrapper  d-flex align-content-center align-content-sm-center">
                    <div className="container">
                        <h4 className="course-details-header mt-5 mb-4"> PHP Course</h4>
                    </div>
                </section>
                <section className="course-detail">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-8 col-sm-12 col-12 col-md-12">
                                <Tabs defaultActiveKey="1" onChange={callback}>
                                    <TabPane tab="Content" key="1">
                                        <Curriculum videos={[]}/>
                                    </TabPane>
                                    <TabPane tab="Description" key="2">
                                        <ThingsToLearn  things_to_learn={ []} description="description"/>
                                    </TabPane>
                                    <TabPane tab="Reviews" key="3">
                                        <DetailsReview/>
                                    </TabPane>
                                    <TabPane tab="Faq" key="4">
                                        <Faq/>
                                    </TabPane>
                                </Tabs>
                            </div>
                            <div className="col-lg-4 col-12 col-md-12">
                                <CourseDetailsSidebar price={12}/>
                            </div>

                        </div>

                    </div>
                </section>
            </div>
        </MainLayout>
    );
};

// export async function getStaticProps() {
//     const data = await getCourseDetails()
//
//     if (!data) {
//         return {
//             notFound: true,
//         }
//     }
//
//     return {
//         props: { data },
//         revalidate: 3600,
//     }
// }

export default Details;