import {BASE_URL} from "../../constants/url";
import axios from "axios";
import {getCookie} from "../../utils";

export const getCourseToWatch = async (slug, token) => {
    return await axios.get(`${BASE_URL}/student/courses/${slug}`, {
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
        },
    })

}
export const finishToWatchVideo = async (slug, video_id) => {
    try {
        return await fetch(`${BASE_URL}/student/courses/${slug}/videos/${video_id}`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie('token'),
            },
            method: "POST",
        });
    } catch (e) {
    }
};

export const markedCourseAsCompleted = async (slug) => {
    try {
        return await fetch(`${BASE_URL}/student/courses/${slug}/completed`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "POST",
        });

    } catch (error) {
        return console.log(error);
    }
};
export const getMyCourses = async (token) => {
    return await axios.get(`${BASE_URL}/student/courses`, {
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
        },
    })

};