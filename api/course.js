import {BASE_URL} from "../constants/url";

export const getAllCourses = async () => {
    try {
        const response = await fetch(`${BASE_URL}/courses`);
        return response.json();
    } catch (error) {

    }
};

export const getCourse = async (slug) => {
    try {
        const course = await fetch(`${BASE_URL}/courses/${slug}`);
        return course.json();
    } catch (e) {
    }
};