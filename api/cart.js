import {BASE_URL} from "../constants/url";
import {getCookie} from "../utils";

export const enrollStudentForPaidCourse = async (card, slug) => {
    try {
        const enrollment = await fetch(`${BASE_URL}/student/courses/${slug}/paid-enrollment`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "POST",
            body: JSON.stringify(card),
        });
        return enrollment;
    } catch (error) {
        return console.log(error);
    }
};

export const enrollStudentForFreeCourse = async (slug) => {
    try {
        const charge = await fetch(`${BASE_URL}/student/courses/${slug}/free-enrollment`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "POST",
        });
        return charge.json();
    } catch (error) {
        return console.log(error);
    }
};