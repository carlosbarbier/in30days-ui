import {BASE_URL} from "../../constants/url";
import {getCookie} from "../../utils";

export const createNewVideo = async (video) => {
    try {
        const response = await fetch(`${BASE_URL}/teacher/videos`, {
            headers: {
                Accept: "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "POST",
            body: video,
        });
        return await response;
    } catch (error) {

    }
};
