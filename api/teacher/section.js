import {BASE_URL} from "../../constants/url";
import {getCookie} from "../../utils";

export const createNewSection = async (slug, section) => {
    try {
        const response = await fetch(`${BASE_URL}/teacher/courses/${slug}/sections`, {
            headers: {
                Accept: "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "POST",
            body: section,
        });
        return await response;
    } catch (error) {

    }
};
