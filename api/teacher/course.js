import {BASE_URL} from "../../constants/url";
import {getCookie} from "../../utils";

export const createNewCourse = async (course) => {
    try {
        const response = await fetch(`${BASE_URL}/teacher/courses`, {
            headers: {
                Accept: "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "POST",
            body: course,
        });
        return await response;
    } catch (error) {

    }
};

export const getMyCourses = async (token) => {
    try {
        return  await fetch(`${BASE_URL}/teacher/courses`, {
            headers: {
                Accept: "application/json",
                Authorization: "Bearer " + token,
            },
            method: "GET",
        });
    } catch (error) {

    }
};
export const getCourseBySlug = async (slug) => {
    try {
        return  await fetch(`${BASE_URL}/teacher/courses/${slug}`, {
            headers: {
                Accept: "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "GET",
        });
    } catch (error) {

    }
};