import {BASE_URL} from "../constants/url";
import {getCookie, removeCookie, removeFromLocalstorage} from "../utils";

export const registerUser = async (user) => {
    try {
        const response = await fetch(`${BASE_URL}/users/register`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(user),
        });
        return await response;
    } catch (error) {

    }
};

export const signInUser = async (data) => {
    try {
        const response = await fetch(`${BASE_URL}/users/login`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(data),
        });
        return await response;
    } catch (error) {

    }
};

export const logout = async (next) => {
    try {
        await fetch(`${BASE_URL}/users/logout`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "POST",
        });
        removeCookie("token");
        removeFromLocalstorage("user");
        next()

    } catch (error) {

    }
};