import * as Yup from "yup";

export const userRegisterSchema = Yup.object().shape({
    name: Yup.string()
        .required('Name is required'),
    email: Yup.string()
        .required('Email is required')
        .email('Email is invalid'),
    password: Yup.string()
        .min(6, 'Password must be at least 6 characters')
        .required('Password is required'),
});

export const userLoginSchema = Yup.object().shape({
    email: Yup.string()
        .required('Email is required')
        .email('Email is invalid'),
    password: Yup.string()
        .required('Password is required'),
});

export const cartSchema = Yup.object().shape({
    expiry_date: Yup.string()
        .matches(/([0-9]{2})\/([0-9]{2})/, 'Not a valid expiration date')
        .required('Expiration date'),
    cvc: Yup.string()
        .matches(/^\d+/, 'Cvc must be a number')
        .required('Cvc is required'),
    card_number: Yup.string()
        .required('Credit Cart number required'),
});

export const courseCreationSchema = Yup.object().shape({
    title: Yup.string()
        .required('Title is required'),
    description: Yup.string()
        .required('Description is required'),
    price: Yup.string()
        .matches(/^\d+/, 'Price must be a number')
        .required('Price is required'),
    programming_language: Yup.string()
        .required('Programming language is required'),
    image: Yup.mixed()
        .test('required', "Image is required", (value) =>{
            return value && value.length
        } )
        .test("fileSize", "The file is too large", (value) => {
            return value && value[0] && value[0].size <= 200000;
        })
        .test("type", "We only support jpeg/png", function (value) {
            return value && value[0] && (value[0].type === "image/png" || value[0].type === "image/jpeg" );
        }),

});

export const lectureSchema = Yup.object().shape({
    title: Yup.string()
        .required('Title is required'),
    video: Yup.mixed()
        .test('required', "Video is required", (value) =>{
            return value && value.length
        } )
        .test("type", "We only suppotr video/mp4", function (value) {
            return value && value[0] && (value[0].type === "video/mp4" );
        }),

});