const path = require("path");
const withAntdLess = require('next-plugin-antd-less');
module.exports = withAntdLess({
  reactStrictMode: true,
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
  modifyVars: {
    '@primary-color': '#0055d4',
    '@border-radius-base': '0.25em'
  },
  lessVarsFilePathAppendToEndOfContent: false,
  cssLoaderOptions: {},
  async rewrites() {
    return [
      {
        source: '/api/:path*',
        destination: 'http://127.0.0.1:5000/api/:path*',
      },
    ]
  },

});
